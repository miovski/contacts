const express = require('express');
const mongoose = require('mongoose');
const { contactValidator, idValidator, searchValidator, validationHandler } = require('./contact/validator')
const path = require('path')
var cors = require('cors');
require('dotenv').config()

const app = express()
//Database
mongoose.connect(process.env.MONGODB_URI ||
                'mongodb://127.0.0.1:27017/contactsdb', {useNewUrlParser: true })
.then(() => console.log("Connected to database"))
.catch(err => console.log(err))
//Middleware
app.use(express.urlencoded({ extended: true}))
app.use(express.json())
app.use(cors());
app.use(express.static('public'))


//Controllers
const contactController = require('./contact/controller')
//Routes
app.post('/api/contact', contactValidator, validationHandler, contactController.create)
app.put('/api/contact', contactValidator, validationHandler, contactController.update)
app.get('/api/contacts/', contactController.retrieveAll)
app.get('/api/contacts/:page/:pagesize', contactController.retrieveByPage)
app.get('/api/contacts/length',contactController.contactsLength)
app.get('/api/contact/:id', idValidator, validationHandler, contactController.retrieveById)
app.post('/api/contacts/search', searchValidator, validationHandler, contactController.search)
app.delete('/api/contact/:id', idValidator, validationHandler, contactController.delete)

app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'))
})
//Start Server
const port = process.env.PORT || 3000;

app.listen(port, ()=> console.log("Server started on 3000"))
