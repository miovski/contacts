import { Injectable } from '@angular/core';
import { Contact } from '../models/Contact';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    contacts: Array<Contact>
    searchOption = []
    searchedContacts = []
    serverUrl = 'https://contacts-vm.herokuapp.com/api'

    constructor(
        private http: HttpClient
    ) { }
   
    search(keyword: String): Observable<Contact[]>{
        return this.http.post<Contact[]>(`${this.serverUrl}/contacts/search`, { keyword: keyword });
    }

    getContactById(id: string): Observable<Contact>{
        return this.http.get<Contact>(`${this.serverUrl}/contact/${id}`);
    }

    getContactsByPage(page: number, pagesize: number): Observable<Contact[]>{
        return this.http.get<Contact[]>(`${this.serverUrl}/contacts/${page}/${pagesize}`);
    }

    getContactsLength(): Observable<Number>{
        return this.http.get<Number>(`${this.serverUrl}/contacts/length`)
    }

    getContacts(): Observable<Contact[]>{
        return this.http.get<Contact[]>(`${this.serverUrl}/contacts`);
    }

    createContact(contact: Contact): Observable<Contact> {
        return this.http.post<Contact>(`${this.serverUrl}/contact`, contact)
    }
    
    updateContact(contact: Contact): Observable<Contact> {
        return this.http.put<Contact>(`${this.serverUrl}/contact`, contact)
    }

    deleteContact(id: string): Observable<Contact> {
        return this.http.delete<Contact>(`${this.serverUrl}/contact/${id}`)
    }
}