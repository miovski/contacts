export class Contact {
    id?: string
    name: string
    last_name: string
    number: string
    birthday: Number
    address: string
}