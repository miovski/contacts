import { Contact } from './Contact'

export class ContactResponse {
    success: boolean
    result: Contact[]
  }
