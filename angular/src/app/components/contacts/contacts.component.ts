import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Contact } from 'src/app/models/Contact';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})


export class ContactsComponent implements OnInit {

	length: Number = 20;
	page = 0
	pageSize = 10;
	pageSizeOptions: number[] = [5, 10, 25, 100];
	hidePaginator = false
	contacts = new Array
	filteredContacts = new Array

 	pageEvent: PageEvent;

	constructor(
		public dataService: DataService,
		public router: Router ) { }

	ngOnInit() {
		this.getData()
	}

	onSelectedFilter(e) {
		this.getFilteredExpenseList();
	}

	getFilteredExpenseList() {
		if (this.dataService.searchOption.length > 0) {
			this.contacts = this.dataService.searchedContacts;
			this.hidePaginator = true
		}
		else {
			this.hidePaginator = false 
			this.dataService.searchedContacts = []
			this.getData();
		}
	}

	getData() {
		this.dataService.getContactsLength().subscribe( 
			(data: Number) => {
				this.length = data
			})
		this.dataService.getContactsByPage(this.page,this.pageSize).subscribe( 
			(data: Contact[]) => {
				this.contacts = data
			})
	}

	editContact(contact){
		this.router.navigateByUrl(`/contact/${contact._id}`);
		this.dataService.searchOption = []
	}

	deleteContact(contact) {
		this.dataService.deleteContact(contact)
		this.getData()
	}

	nextPage(event?:PageEvent){
		this.page = event.pageIndex
		this.dataService.getContactsByPage(this.page,this.pageSize).subscribe( 
			(data: Contact[]) => {
				this.contacts = data
			})
	}

}
