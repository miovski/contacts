import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Contact } from 'src/app/models/Contact';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

	contactForm : FormGroup
	submitted = false
	loading = false
	error = ''
	serverError = ''
	contact: Contact
	contactId;
  
	constructor(
		private dataService: DataService,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router
		) { }

	ngOnInit() {
		this.contactForm = this.formBuilder.group({
			name: ['', Validators.required],
			last_name: ['', Validators.required],
			number: ['', Validators.required],
			birthday: ['', Validators.required],
			address: ['', Validators.required]
		})
  
		this.route.params.subscribe( params => {
			if(params.id){
				this.contactId = params.id
				this.dataService.getContactById(params.id).subscribe( 
					(contact : Contact) => {
						this.contact = contact
						this.contactForm.patchValue(this.contact)
						this.contactForm.controls.birthday.setValue(this.convertTimestampToDate(this.contact.birthday))
					},(error) => {
						this.router.navigateByUrl('')
				})
			}
		});
  }

	updateContact(){
		this.submitted = true
		if (this.contactForm.invalid) return

		let contact = {
			name: this.contactForm.controls.name.value,
			last_name: this.contactForm.controls.last_name.value,
			number: this.contactForm.controls.number.value,
			birthday: this.convertDateToTimestamp(this.contactForm.controls.birthday.value),
			address: this.contactForm.controls.address.value,
		}

		this.dataService.updateContact(contact).subscribe(
			(data:Contact) => {
				if(data) this.router.navigateByUrl('')
			},
			(error) => {
				this.serverError = error.error.result
			})
	}

	deleteContact(){
		this.dataService.deleteContact(this.contactId).subscribe(
			(data:any) => {
				if(data.success) this.router.navigateByUrl('') 
				else this.serverError = data.result
			}
		)
	}

	createContact(){
		this.submitted = true
		if (this.contactForm.invalid) return

		let contact = {
			name: this.contactForm.controls.name.value,
			last_name: this.contactForm.controls.last_name.value,
			number: this.contactForm.controls.number.value,
			birthday: this.convertDateToTimestamp(this.contactForm.controls.birthday.value),
			address: this.contactForm.controls.address.value,
		}
		
		this.dataService.createContact(contact).subscribe( 
			(data:Contact) => {
				if(data) 
					this.router.navigateByUrl('');
				},
				(error) => {
					this.serverError = error.error.result
				});
	}
  

	getContactFromForm = ():Contact => {
		return {
			name: this.contactForm.controls.name.value,
			last_name: this.contactForm.controls.last_name.value,
			number: this.contactForm.controls.number.value,
			birthday: this.convertDateToTimestamp(this.contactForm.controls.birthday.value),
			address: this.contactForm.controls.address.value,
		}
	}

  	convertDateToTimestamp = (date) => Date.parse(date)

	convertTimestampToDate = (timestamp) => {
		let date = new Date(Number(timestamp))
		let formattedDate =  
			date.getFullYear()+'-'+ 
			('0'+(date.getMonth() + 1)).slice(-2) +'-'+ 
			('0' + date.getDate()).slice(-2);
			
		return formattedDate
	}

}
