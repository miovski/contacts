import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Contact } from 'src/app/models/Contact';
import { DataService } from 'src/app/services/data.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

    myControl = new FormControl();
    filteredOptions: Observable<string[]>;
    contacts: Contact[];
    autoCompleteList: any[]

    @ViewChild('autocompleteInput') autocompleteInput: ElementRef;
    @Output() onSelectedOption = new EventEmitter();

    constructor(
        public dataService: DataService) { }

    ngOnInit() {
        // when user types something in input, the value changes will come through this
        this.myControl.valueChanges.pipe(debounceTime(500)).subscribe(userInput => {
        if(userInput.length >= 2) {
            this.dataService.search(userInput).subscribe( (data:any) => {
                console.log(data)
                this.dataService.contacts = data.result
                this.autoCompleteList = data.result
            })
        } else this.autoCompleteList = [] 
        })
    }

    // after you clicked an autosuggest option, this function will show the field you want to show in input
    displayFn(contact: Contact) {
        let k = contact ? contact.name : contact;
        return k;
    }

    filterContactList(contact) {
        this.dataService.searchOption.push(contact.name);
        this.dataService.searchedContacts.push(contact)
        this.onSelectedOption.emit(this.dataService.searchOption)
        this.focusOnPlaceInput();
    }

    removeOption(option) {
        let index = this.dataService.searchOption.indexOf(option);
        if (index >= 0) {
            this.dataService.searchedContacts.splice(index, 1)
            this.dataService.searchOption.splice(index, 1);
            this.autoCompleteList = []
            this.focusOnPlaceInput();
            this.onSelectedOption.emit(this.dataService.searchOption)
        }
    }

    // focus the input field and remove any unwanted text.
    focusOnPlaceInput() {
        this.autocompleteInput.nativeElement.focus();
        this.autocompleteInput.nativeElement.value = '';
    }

}
