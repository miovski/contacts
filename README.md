# Contact App

Contacts App is a SPA app that manages contacts created with Angular which is connected to NodeJS server.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes. See deployment for notes on how to deploy the project on a live system.

For live demo check out

* [Contacts](https://contacts-vm.herokuapp.com/) - Live demo of the application

### Installing

You need to install the required modules and how to install them.
A step by step series of examples that tell you how to get a development env running

Install NodeJS modules with

```
npm install
```

NExt step, go in angular folder, install modules for the angular app

```
cd angular
npm install
```

Now everything should be installed.

## Running the server

```
npm start
```


## Deployment

This application is deployed on heroku. The Angular is built then the html is returned via api from the NodeJs server as static file.

## Built With

* [Angular](https://angular.io/) - The web framework used
* [Angular Material](https://material.angular.io/) - Material design components for Angular

* [NodeJS](https://nodejs.org/en/) - Node.js is an open-source, cross-platform, JavaScript runtime environment that executes JavaScript code outside of a web browser.
* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [MongoDB](https://www.mongodb.com/) - Database

## Authors

* **Miovski Vlatko** - *Initial work* -

## License

This project is licensed under the MIT License
