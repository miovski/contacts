const mongoose = require('mongoose');
const Schema = mongoose.Schema

const ContactSchema = new Schema({
    name: String,
    last_name: String,
    number: String,
    birthday: Number,
    address: String,
})
module.exports = mongoose.model('contact', ContactSchema)