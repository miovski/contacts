const { body, param, validationResult } = require('express-validator');

module.exports.contactValidator = [
    body('name').isString(),
    body('last_name').isString(),
    body('number').isString(),
    body('birthday').isNumeric(),
    body('address').isString()
]
module.exports.idValidator = [
    param('id').isString()
]
module.exports.searchValidator = [
    body('keyword').isString()
]
module.exports.validationHandler = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    return next()
}