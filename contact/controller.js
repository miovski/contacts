const Contact = require('./model');

module.exports = {

    create: (req, res) => {
        Contact.findOne({ number: req.body.number })
        .then(result => {
            if(result) return res.status(404).json({ success: false, result: "Contact with the same number already exists"})
            let contact = new Contact({
                name: req.body.name,
                last_name: req.body.last_name,
                number: req.body.number,
                birthday: req.body.birthday,
                address: req.body.address
            })
            contact.save()
            .then(result => {
                return res.status(200).json({ success: true, result: result})
            })
            .catch(err => {
                return res.status(500).json({ success: false, result: err})
            })
        
        })
        .catch(err => {
            return res.status(500).json({ success: false, result: err})
        }) 
        
    },

    update: (req, res) => {
        Contact.updateOne({ number: req.body.number },{$set: {
            name: req.body.name,
            last_name: req.body.last_name,
            number: req.body.number,
            birthday: req.body.birthday,
            address: req.body.address
        }}, {new: true, useFindAndModify: false})
        .then(contact => {
            if (!contact) return res.status(404).json({ success: false, result: "No such contact exists" })
            return res.status(200).json(contact)
        })
      .catch(err => {
          return res.status(500).json(err)
      })
    },

    retrieveAll: (req, res) => {
        Contact.find().sort({name: 'asc'})
        .then(contacts => {
            if (!contacts) return res.status(404).json({ success: false, result: "No contacts found"})
            return res.status(200).json(contacts)
        })
        .catch(err => {
            console.log(err)
            return res.status(500).json({ success: false, result: err})
        })  
    },

    retrieveByPage: (req, res) => {
        let from = req.params.page * req.params.pagesize
        Contact.find().sort({name: 'asc'}).skip(Number(from)).limit(Number(req.params.pagesize))
        .then(contacts => {
            if (!contacts) return res.status(404).json({ success: false, result: "No contacts found"})
            return res.status(200).json(contacts)
        })
        .catch(err => {
            console.log(err)
            return res.status(500).json({ success: false, result: err})
        })  
    },

    contactsLength: (req, res) => {
        Contact.find()
        .then(contacts => {
            if (!contacts) return res.status(404).json({ success: false, result: "No contacts found"})
            return res.status(200).json(contacts.length)
        })
        .catch(err => {
            console.log(err)
            return res.status(500).json({ success: false, result: err})
        })  
    },
    
    retrieveById: (req, res) => {
        Contact.findOne( { _id: req.params.id })
        .then(contact => {
            if (!contact) return res.status(404).json({ success: false, result: "No contact found"})
            return res.status(200).json(contact)
        })
        .catch(err => {
            console.log(err)
            return res.status(500).json({ success: false, result: err})
        })  
    },

    search: (req, res) => {
        Contact.find()
        .then(result => {
            if (!result) return res.status(404).json({ success: false, result: "No contacts found"})
            let matchedResult = result.filter(res => 
                res.name.toLowerCase().indexOf(req.body.keyword.toLowerCase()) !=-1 ||
                res.last_name.toLowerCase().indexOf(req.body.keyword.toLowerCase()) != -1)   
            return res.status(200).json({ sucess: true, result: matchedResult})
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({ success: false, result: err})
        })
    },


    delete: (req, res) => {
        Contact.deleteOne({ _id: req.params.id}, (err) => {
            if(err) return res.status(404).json({ success: false, result: "No contact with such ID was found." })
            return res.status(200).json({ success: true, result: "Contact was deleted!" })
        })
    }
}